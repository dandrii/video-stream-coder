#include <VideoStreamCoderLib.h>
#include <VideoStreamCoderLib_export.h>
#include <opencv2/opencv.hpp>

extern "C" {
#include <libavutil/frame.h>
}

void show_frame(AVFrame* frame)
{
	if (frame->height > 0 && frame->width > 0)
	{
		cv::Mat preview = cv::Mat(frame->height, frame->width, CV_8UC3, frame->data[0], frame->linesize[0]);
		cv::rotate(preview, preview, cv::ROTATE_90_CLOCKWISE);

		cv::namedWindow("Preview", cv::WINDOW_NORMAL);
		cv::imshow("Preview", preview);
	}

	cv::waitKey(1);
}

void main()
{
	Coder* c = CreateCoder();
	c->SetCallback(show_frame);
	c->Join();
}