#include <VideoStreamCoderLib.h>
#include <VideoStreamCoderLib_export.h>
#include <opencv2/opencv.hpp>

#include <tensorflow/lite/interpreter.h>
#include <tensorflow/lite/kernels/register.h>
#include <tensorflow/lite/model.h>

#include "transpose_conv_bias.h"

#include <chrono>

extern "C" {
#include <libavutil/frame.h>
}

char basePath[_MAX_PATH];

class App
{
private:
	cv::HOGDescriptor hog;
	std::vector<cv::Rect> people;
	std::vector<double> weights;

	cv::Mat lastFrame;
	cv::Mat processingFrame;

	const int modelSize = 256 * 256 * 3;

	std::unique_ptr<tflite::Interpreter> interpreter;
	std::unique_ptr<tflite::FlatBufferModel> model;

	std::mutex mtx;

	void ProcessFrame()
	{
		auto start = std::chrono::steady_clock::now();
		cv::Mat originalFrame = cv::Mat(processingFrame);

		cv::resize(processingFrame, processingFrame, cv::Size(256, 256));
		cv::normalize(processingFrame, processingFrame, 0, 1, cv::NORM_MINMAX, CV_32FC2);

		auto input = interpreter->typed_input_tensor<float>(0);
		float* processingFrameData = (float*)processingFrame.data;
		for (int y = 0; y < processingFrame.rows; y++)
		{
			for (int x = 0; x < processingFrame.cols; x++)
			{
				int i = y * processingFrame.cols * 3 + x * 3;
				input[i] = processingFrameData[i + 2];
				input[i + 1] = processingFrameData[i + 1];
				input[i + 2] = processingFrameData[i];
			}
		}

		if (interpreter->Invoke() != kTfLiteOk)
		{
			throw std::runtime_error("interpreter cannot be invoked");
		}

		auto output = interpreter->typed_output_tensor<float>(0);

		cv::Mat mask = cv::Mat(256, 256, CV_32FC1, output);
		cv::imshow("Mask", mask);

		int width = 500;

		float r = static_cast<float>(originalFrame.cols) / static_cast<float>(originalFrame.rows);
		cv::resize(originalFrame, originalFrame, cv::Size(width, width / r));
		cv::resize(mask, mask, cv::Size(width, width / r));

		// cv::resize(mask, mask, cv::Size(originalFrame.cols, originalFrame.rows

		uchar* originalFrameData = (uchar*)originalFrame.data;
		float* alphaData = (float*)mask.data;
		for (int i = 0; i < mask.rows * mask.cols; i++)
		{
			originalFrameData[i * 3] = originalFrameData[(i * 3)] * alphaData[i];
			originalFrameData[(i * 3) + 1] = originalFrameData[(i * 3) + 1] * alphaData[i];
			originalFrameData[(i * 3) + 2] = originalFrameData[(i * 3) + 2] * alphaData[i];
		}
		cv::imshow("Preview", originalFrame);

		cv::waitKey(1);

		auto end = std::chrono::steady_clock::now();
		// std::cout << "Frame ETA: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << std::endl;
	}

	void GetResourcePath(char* dest, const char* filename)
	{
		sprintf(dest, "%s\\%s", basePath, filename);
	}

public:
	App()
	{
		char path[_MAX_PATH];
		GetResourcePath(path, "selfie_segmentation.tflite");
		model = tflite::FlatBufferModel::BuildFromFile(path);
		if (model == nullptr)
		{
			throw std::runtime_error("model cannot be initialised");
		}

		tflite::ops::builtin::BuiltinOpResolver resolver;
		resolver.AddCustom("Convolution2DTransposeBias", mediapipe::tflite_operations::RegisterConvolution2DTransposeBias());
		if (tflite::InterpreterBuilder(*model, resolver)(&interpreter) != kTfLiteOk)
		{
			throw std::runtime_error("interpreter cannot be resolved");
		}

		interpreter->AllocateTensors();
	}

	void OnFrame(AVFrame* frame)
	{
		if (frame->height > 0 && frame->width > 0)
		{
			if (mtx.try_lock())
			{
				lastFrame = cv::Mat(frame->height, frame->width, CV_8UC3, frame->data[0], frame->linesize[0]);
				mtx.unlock();
			}
		}
	}

	void Loop()
	{
		while (true)
		{
			mtx.lock();
			if (!lastFrame.empty())
			{
				lastFrame.copyTo(processingFrame);
				mtx.unlock();
				ProcessFrame();
			}
			else
			{
				mtx.unlock();
			}
		}
	}
};



void main(int argc, char* argv[])
{
	char cs[_MAX_PATH];
	_fullpath(cs, argv[0], _MAX_PATH);
	std::string absPath(cs);
	absPath.substr(0, absPath.find_last_of("\\/")).copy(basePath, _MAX_PATH, 0);

	App* app = new App();
	std::thread t1(&App::Loop, app);

	Coder* c = new Coder();
	c->SetCallback([=](AVFrame* frame) {
		app->OnFrame(frame);
		});
	c->Join();
	t1.join();
}