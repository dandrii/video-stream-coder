#include "net.h"

bool set_nonblock(SOCKET sock, int nb)
{
	u_long nbl = nb;
	return (NO_ERROR == ioctlsocket(sock, FIONBIO, &nbl));
}

void log_last_error()
{
	int err = WSAGetLastError();
	LPTSTR errorText = NULL;

	FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM
		| FORMAT_MESSAGE_ALLOCATE_BUFFER
		| FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		err,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&errorText,
		0,
		NULL);

	if (NULL != errorText)
	{
		std::cerr << "socket error " << err << " " << errorText << std::endl;

		LocalFree(errorText);
		errorText = NULL;
	}
}

SOCKET tcp_connect(const char* ip, uint16_t port)
{
	int len;
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == INVALID_SOCKET)
	{
		log_last_error();
		return INVALID_SOCKET;
	}

	SOCKADDR_IN sin{};
	sin.sin_family = AF_INET;
	inet_pton(AF_INET, ip, &sin.sin_addr.s_addr);
	sin.sin_port = htons(port);

	struct timeval timeout = { 2, 0 };

	fd_set set{};
	FD_ZERO(&set);
	FD_SET(sock, &set);

	if (!set_nonblock(sock, 1))
	{
		net_close(sock);
		return INVALID_SOCKET;
	}

	connect(sock, (SOCKADDR*)&sin, sizeof(sin));
	if (WSAGetLastError() != WSAEWOULDBLOCK)
	{
		net_close(sock);
		return INVALID_SOCKET;
	}

	if (select(sock + 1, NULL, &set, NULL, &timeout) <= 0)
	{
		log_last_error();
		net_close(sock);
		return INVALID_SOCKET;
	}

	if (!set_nonblock(sock, 0))
	{
		net_close(sock);
		return INVALID_SOCKET;
	}

	len = 65536 * 4;
	setsockopt(sock, SOL_SOCKET, SO_RCVBUF, (char*)&len, sizeof(int));

	DWORD timeout_dw = 5000;
	setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout_dw, sizeof(timeout));
	return sock;
}

SOCKET udp_socket(uint16_t port)
{
	SOCKET sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock == INVALID_SOCKET)
	{
		log_last_error();
		return INVALID_SOCKET;
	}

	char broadcast = '1';
	if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(char)) < 0)
	{
		std::cerr << "cannot set broadcast option" << std::endl;
		net_close(sock);
		return INVALID_SOCKET;
	}

	SOCKADDR_IN sin{};
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = INADDR_ANY;

	if (bind(sock, (SOCKADDR*)&sin, sizeof(sin)) < 0)
	{
		std::cerr << "cannot bind socket" << std::endl;
		log_last_error();
		net_close(sock);
		return INVALID_SOCKET;
	}

	return sock;
}

bool net_close(SOCKET sock)
{
	shutdown(sock, SD_BOTH);
	return !closesocket(sock);
}
