#pragma once

#include <stdint.h>
#include <istream>
#include <iostream>
#include <WinSock2.h>
#include <WS2tcpip.h>

SOCKET tcp_connect(const char* ip, uint16_t port);
SOCKET udp_socket(uint16_t port);

bool net_close(SOCKET sock);
