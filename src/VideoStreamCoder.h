﻿#pragma once

#include <thread>
#include <functional>
#include <set>
#include "decoder.h"
#include "VideoStreamCoderLib_Export.h"

typedef std::function<void(AVFrame*)> frame_cb;

class Coder {
private:
	bool is_running_;

	std::set<char*>* servers_;
	FFMpegDecoder* decoder_;

	std::thread* t_receive_;
	std::thread* t_decode_;
	std::thread* t_listen_;

	frame_cb on_frame_cb_;

	void Decode();
	void Receive();
	void ListenToBroadcast();

public:
	void VideoStreamCoderLib_EXPORT SetCallback(frame_cb);

	void VideoStreamCoderLib_EXPORT Stop();

	void VideoStreamCoderLib_EXPORT Join();

	VideoStreamCoderLib_EXPORT Coder();
	VideoStreamCoderLib_EXPORT ~Coder();
};

extern "C" {
	VideoStreamCoderLib_EXPORT Coder* CreateCoder();
}
