﻿#include <iostream>
#include <WinSock2.h>

#include "net.h"
#include "VideoStreamCoder.h"

using namespace std::chrono_literals;

constexpr auto VERSION = 1;

constexpr auto MAX_PACKET_SIZE = 1024 * 1024;
constexpr auto HEADER_SIZE = 12;

static inline
uint32_t buffer_read32be(const uint8_t* buf)
{
	return (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3];
}

static inline
void buffer_write32be(uint8_t* buf, const uint32_t n, const uint32_t offset)
{
	byte b;

	for (int i = 3; i >= 0; i--)
	{
		b = (n >> 8 * i) & 0xFF;
		*(buf + offset + (3 - i)) = b;
	}
}

static inline
uint64_t buffer_read64be(const uint8_t* buf)
{
	uint32_t msb = buffer_read32be(buf);
	uint32_t lsb = buffer_read32be(&buf[4]);
	return ((uint64_t)msb << 32) | lsb;
}

static CoderDataPacket* read_frame(FFMpegDecoder* dec, SOCKET sock, bool* got_error)
{
	uint8_t header[HEADER_SIZE]{};
	int r;
	size_t len = 0;
	uint64_t pts;

	r = recv(sock, (char*)header, HEADER_SIZE, MSG_WAITALL);
	if (r != HEADER_SIZE)
	{
		if (r == 0)
		{
			std::cout << "socket likeky disconnected" << std::endl;
			*got_error = true;
		}
		else
		{
			std::cout << "reader returned " << r << std::endl;
		}

		return NULL;
	}

	pts = buffer_read64be(header);
	len = buffer_read32be(&header[8]);
	// std::cout << "header pts=" << pts << " len=" << len << std::endl;

	if (len == 0 || len > MAX_PACKET_SIZE)
	{
		std::cout << "packet length invalid, len=" << len << std::endl;
		return NULL;
	}

	CoderDataPacket* dp = dec->pull_empty_packet(len);
	uint8_t* p = dp->data;

	r = recv(sock, (char*)p, len, MSG_WAITALL);
	if (r != len)
	{
		std::cout << "couldn't read enought bytes, r=" << r << " len=" << len << std::endl;
		dec->push_empty_packet(dp);
		return NULL;
	}

	dp->pts = pts;
	dp->used = len;
	return dp;
}

void Coder::Decode()
{
	FFMpegDecoder* dec = NULL;
	CoderDataPacket* dp = NULL;

	bool got_output;

	while (is_running_)
	{
		if ((dec = decoder_) == NULL || (dp = dec->pull_ready_packet()) == NULL)
		{
			std::this_thread::sleep_for(2ms);
			continue;
		}

		if (dec->failed)
		{
			dec->push_empty_packet(dp);
			continue;
		}

		if (!dec->decode_video(dp, &got_output))
		{
			std::cout << "error decoding video" << std::endl;
			// dec->failed = true;
			// dec->push_empty_packet(dp);
			continue;
		}

		if (got_output && on_frame_cb_ != NULL)
		{
			on_frame_cb_(dec->bgr_frame);
		}

		decoder_->push_empty_packet(dp);
	}
}

void Coder::Receive()
{
	FFMpegDecoder* dec;
	bool got_error;

	uint8_t video_req[16];
	buffer_write32be(video_req, 1920, 0);
	buffer_write32be(video_req, 1080, 4);
	buffer_write32be(video_req, VERSION, 8);
	buffer_write32be(video_req, rand() % 9000 + 1000, 12);

	SOCKET sock = INVALID_SOCKET;

wait_again:
	while (servers_->empty())
	{
		std::this_thread::sleep_for(5s);
	}

	std::set<char*>::iterator ip = servers_->begin();

	if ((sock = tcp_connect(*ip, 8914)) == INVALID_SOCKET)
	{
		std::cout << "cannot connect to the server" << std::endl;
		servers_->erase(ip);
		goto wait_again;
	}


	int w = 0;
	char* ptr = (char*)video_req;
	while (w < 16)
	{
		w += send(sock, ptr, 16, 0);
		if (w <= 0)
		{
			std::cout << "receive thread exited, cannot request data from the server" << std::endl;
			net_close(sock);
			servers_->erase(ip);
			goto wait_again;
		}
		ptr += w;
	};

	if (!(dec = decoder_))
	{
		dec = new FFMpegDecoder();

		if (dec->init() < 0)
		{
			std::cout << "receive thread exited, cannot initialise decoder" << std::endl;
			Stop();
			throw std::runtime_error("cannot initialise decoder");
		}

		decoder_ = dec;
	}

	while (is_running_)
	{
		CoderDataPacket* dp = read_frame(dec, sock, &got_error);
		if (dec->failed)
		{
			std::cout << "decoder failed" << std::endl;
			dec->push_empty_packet(dp);
			continue;
		}

		if (dp)
		{
			dec->push_ready_packet(dp);
		}
		else if (got_error)
		{
			net_close(sock);
			servers_->erase(ip);
			delete decoder_;
			goto wait_again;
		}
	}

	if (sock != INVALID_SOCKET)
	{
		net_close(sock);
	}
}

void Coder::ListenToBroadcast()
{
	SOCKET sock;

	if ((sock = udp_socket(8915)) == INVALID_SOCKET)
	{
		std::cout << "listen_to_broadcast thread exited, cannot start the socket";
		Stop();
		throw std::runtime_error("cannot start broadcast socket");
	}

	char recvbuff[16];
	SOCKADDR_IN sin{};
	int cinlen = sizeof(sin);

	while (is_running_)
	{
		recvfrom(sock, recvbuff, 16, 0, (SOCKADDR*)&sin, &cinlen);

		std::cout << "client found, message => " << recvbuff << std::endl;
		if (strcmp(recvbuff, "video_srv:greet") == 0)
		{
			char ip[INET_ADDRSTRLEN];
			inet_ntop(AF_INET, &sin.sin_addr.s_addr, ip, sizeof(ip));
			servers_->insert(ip);
			std::cout << "client connected, address => " << ip << std::endl;
		}
	}
}

Coder::Coder()
{
	WSADATA wsa;
	int res = WSAStartup(MAKEWORD(2, 2), &wsa) < 0;
	if (res < 0)
	{
		std::cout << "init thread exited, WSAStartup failed " << res << std::endl;
		throw std::runtime_error("WSAStartup failed");
	}

	decoder_ = NULL;
	is_running_ = true;

	servers_ = new std::set<char*>();

	t_receive_ = new std::thread(&Coder::Receive, this);
	t_decode_ = new std::thread(&Coder::Decode, this);
	t_listen_ = new std::thread(&Coder::ListenToBroadcast, this);
}

Coder::~Coder()
{
	is_running_ = false;

	if (decoder_ != NULL)
	{
		delete decoder_;
	}
	if (servers_ != NULL)
	{
		delete servers_;
	}

	if (t_receive_ != NULL)
	{
		delete t_receive_;
	}
	if (t_decode_ != NULL)
	{
		delete t_decode_;
	}
	if (t_listen_ != NULL)
	{
		delete t_listen_;
	}
}

void Coder::SetCallback(frame_cb on_frame_cb)
{
	this->on_frame_cb_ = on_frame_cb;
}

void Coder::Stop()
{
	is_running_ = false;
}

void Coder::Join()
{
	t_receive_->join();
	t_decode_->join();
	t_listen_->join();
}

Coder* CreateCoder()
{
	return new Coder();
}
