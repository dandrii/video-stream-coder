#pragma once

#include <vector>
#include <mutex>
#include <iostream>
#include <fstream>

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

#if LIBAVCODEC_VERSION_MAJOR >= 58
#define CODEC_CAP_TRUNC AV_CODEC_CAP_TRUNCATED
#define CODEC_FLAG_TRUNC AV_CODEC_FLAG_TRUNCATED
#define INPUT_BUFFER_PADDING_SIZE AV_INPUT_BUFFER_PADDING_SIZE
#else
#define CODEC_CAP_TRUNC CODEC_CAP_TRUNCATED
#define CODEC_FLAG_TRUNC CODEC_FLAG_TRUNCATED
#define INPUT_BUFFER_PADDING_SIZE FF_INPUT_BUFFER_PADDING_SIZE
#endif

struct CoderDataPacket
{
	uint8_t* data;
	size_t size;
	size_t used;
	uint64_t pts;

	CoderDataPacket(size_t new_size)
	{
		size = 0;
		data = 0;
		resize(new_size);
	}

	~CoderDataPacket(void)
	{
		if (data) av_free(data);
	}

	void resize(size_t new_size)
	{
		if (size < new_size)
		{
			data = (uint8_t*)av_realloc(data, new_size);
			size = new_size;
		}
	}
};

template<typename T>
struct Queue
{
	std::mutex items_lock;
	std::vector<T> items;

	void add_item(T item)
	{
		items_lock.lock();
		items.push_back(item);
		items_lock.unlock();
	}

	T next_item(void)
	{
		T item{};
		if (items.size())
		{
			items_lock.lock();
			item = items.front();
			items.erase(items.begin());
			items_lock.unlock();
		}
		return item;
	}
};

struct FFMpegDecoder
{
	Queue<CoderDataPacket*> recieveQueue;
	Queue<CoderDataPacket*> decodeQueue;
	size_t alloc_count;
	bool ready;
	bool failed;

	int current_frame;

	AVCodecContext* decoder;
	AVCodec* codec;
	AVPacket* packet_;
	AVBufferRef* hw_ctx;
	AVFrame* frame_hw;
	AVFrame* frame;
	AVFrame* out_frame;
	AVFrame* bgr_frame;
	enum AVPixelFormat hw_pix_fmt;
	bool hw;
	bool catchup;
	bool b_frame_check;

	SwsContext* convert_ctx;

	FFMpegDecoder(void)
	{
		alloc_count = 0;
		ready = false;
		failed = false;
		current_frame = 0;

		decoder = NULL;
		codec = NULL;
		packet_ = NULL;
		hw_ctx = NULL;
		frame = NULL;
		frame_hw = NULL;
		hw_pix_fmt = AV_PIX_FMT_NONE;
		hw = false;
		catchup = false;
		b_frame_check = false;
		convert_ctx = NULL;
		bgr_frame = NULL;
		out_frame = NULL;
	}

	~FFMpegDecoder(void)
	{
		CoderDataPacket* _packet;
		while ((_packet = recieveQueue.next_item()) != NULL)
		{
			delete _packet;
			alloc_count--;
		}
		while ((_packet = decodeQueue.next_item()) != NULL)
		{
			delete _packet;
			alloc_count--;
		}

		if (frame_hw)
		{
			av_frame_unref(frame_hw);
			av_free(frame_hw);
		}

		if (frame)
		{
			av_frame_unref(frame);
			av_free(frame);
		}

		if (bgr_frame)
		{
			av_frame_unref(bgr_frame);
			av_free(bgr_frame);
		}

		if (hw_ctx)
			av_buffer_unref(&hw_ctx);

		if (packet_)
			av_packet_free(&packet_);

		if (decoder)
			avcodec_free_context(&decoder);

		if (convert_ctx)
			sws_freeContext(convert_ctx);
	}

	inline CoderDataPacket* pull_ready_packet(void)
	{
		return decodeQueue.next_item();
	}

	inline void push_empty_packet(CoderDataPacket* packet)
	{
		recieveQueue.add_item(packet);
	}

	int init();
	bool decode_video(CoderDataPacket*, bool* got_output);
	bool convert_to_bgr();

	CoderDataPacket* pull_empty_packet(size_t);
	void push_ready_packet(CoderDataPacket*);
};