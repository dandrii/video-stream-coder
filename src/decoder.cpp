#include "decoder.h"

#if LIBAVCODEC_VERSION_INT < AV_VERSION_INT(58, 9, 100)
#error LIBAVCODEC VERSION 58,9,100 REQUIRED
#endif

enum AVHWDeviceType hw_device_list[] = {
	AV_HWDEVICE_TYPE_D3D11VA,
	AV_HWDEVICE_TYPE_NONE,
};

static AVPixelFormat has_hw_type(AVCodec* c, enum AVHWDeviceType type)
{
	for (int i = 0;; i++)
	{
		const AVCodecHWConfig* config = avcodec_get_hw_config(c, i);
		if (!config)
		{
			break;
		}

		if ((config->methods & AV_CODEC_HW_CONFIG_METHOD_HW_DEVICE_CTX) && config->device_type == type)
		{
			return config->pix_fmt;
		}
	}

	return AV_PIX_FMT_NONE;
}

static void init_hw_decoder(FFMpegDecoder* d)
{
	enum AVHWDeviceType* hw_device_iter = hw_device_list;
	AVBufferRef* hw_ctx = NULL;

	while (*hw_device_iter != AV_HWDEVICE_TYPE_NONE)
	{
		d->hw_pix_fmt = has_hw_type(d->codec, *hw_device_iter);
		if (d->hw_pix_fmt != AV_PIX_FMT_NONE)
		{
			if (av_hwdevice_ctx_create(&hw_ctx, *hw_device_iter, NULL, NULL, 0) == 0)
				break;

			d->hw_pix_fmt = AV_PIX_FMT_NONE;
		}

		hw_device_iter++;
	}

	if (hw_ctx)
	{
		d->decoder->hw_device_ctx = av_buffer_ref(hw_ctx);
		d->hw_ctx = hw_ctx;
		d->hw = true;
	}
}

int FFMpegDecoder::init()
{
	int ret;

	codec = avcodec_find_decoder(AV_CODEC_ID_HEVC);
	if (!codec)
		return -1;

	decoder = avcodec_alloc_context3(codec);
	decoder->opaque = this;

	init_hw_decoder(this);

	ret = avcodec_open2(decoder, codec, NULL);
	if (ret < 0)
	{
		return ret;
	}

	decoder->flags |= AV_CODEC_FLAG_TRUNCATED;
	decoder->flags |= AV_CODEC_FLAG_LOW_DELAY;
	decoder->flags2 |= AV_CODEC_FLAG2_FAST;
	decoder->flags2 |= AV_CODEC_FLAG2_CHUNKS;
	decoder->thread_type = FF_THREAD_SLICE;

	frame = av_frame_alloc();
	if (!frame)
		return -1;

	if (hw)
	{
		frame_hw = av_frame_alloc();
		if (!frame_hw) hw = false;
	}

	packet_ = av_packet_alloc();
	if (!packet_)
		return -1;

	ready = true;
	return 0;
}

CoderDataPacket* FFMpegDecoder::pull_empty_packet(size_t size)
{
	size_t new_size = size + INPUT_BUFFER_PADDING_SIZE;
	CoderDataPacket* packet = recieveQueue.next_item();
	if (!packet)
	{
		packet = new CoderDataPacket(new_size);
		alloc_count++;
	}
	else
	{
		packet->resize(new_size);
	}
	packet->used = 0;
	memset(packet->data, 0, new_size);
	return packet;
}

void FFMpegDecoder::push_ready_packet(CoderDataPacket* packet)
{
	if (catchup)
	{
		if (decodeQueue.items.size() > 0)
		{
			recieveQueue.add_item(packet);
			return;
		}

		// discard P/B frames
		int nalType = packet->data[2] == 1 ? (packet->data[3] & 0x1f) : (packet->data[4] & 0x1f);
		if (nalType < 5)
		{
			// std::cout << "discard non-keyframe" << std::endl;
			// recieveQueue.add_item(packet);
			// return;
		}

		std::cout << "decoder catchup: decodeQueue: " << decodeQueue.items.size() << " recieveQueue: " << recieveQueue.items.size() << std::endl;
		catchup = false;
	}

	decodeQueue.add_item(packet);

	if (decodeQueue.items.size() > 25)
	{
		catchup = true;
	}
}

bool FFMpegDecoder::convert_to_bgr()
{
	if (!convert_ctx)
	{
		convert_ctx = sws_getContext(decoder->width, decoder->height, AV_PIX_FMT_NV12, decoder->width, decoder->height, AV_PIX_FMT_BGR24, SWS_BICUBIC, NULL, NULL, NULL);
	}

	if (!convert_ctx)
	{
		std::cout << "convert_ctx is null" << std::endl;
		return false;
	}

	if (!bgr_frame)
	{
		bgr_frame = av_frame_alloc();
		if (!bgr_frame)
		{
			std::cout << "framergb is null" << std::endl;
			return false;
		}

		av_image_alloc(bgr_frame->data, bgr_frame->linesize, decoder->width, decoder->height, AV_PIX_FMT_BGR24, 32);
	}

	sws_scale(convert_ctx, out_frame->data, out_frame->linesize, 0, decoder->height, bgr_frame->data, bgr_frame->linesize);
	bgr_frame->height = decoder->height;
	bgr_frame->width = decoder->width;

	return true;
}

bool FFMpegDecoder::decode_video(CoderDataPacket* dp, bool* got_output)
{
	int ret;
	*got_output = false;

	packet_->data = dp->data;
	packet_->size = dp->used;
	packet_->pts = AV_NOPTS_VALUE;

	if (decoder->has_b_frames && !b_frame_check)
	{
		std::cout << "stream has b-frames" << std::endl;
		b_frame_check = true;
	}

	ret = avcodec_send_packet(decoder, packet_);
	if (ret != 0)
	{
		std::cout << "avcodec_send_packet failed with " << ret << std::endl;
		return ret == AVERROR(EAGAIN);
		// return true;
	}

	out_frame = hw ? frame_hw : frame;
	while (!ret)
	{
		ret = avcodec_receive_frame(decoder, out_frame);
		if (ret != 0)
		{
			if (ret != AVERROR(EAGAIN))
			{
				std::cout << "avcodec_receive_frame failed with " << ret << std::endl;
			}
			return ret == AVERROR(EAGAIN);
		}

		if (hw)
		{
			if (frame_hw->format == hw_pix_fmt)
			{
				if ((ret = av_hwframe_transfer_data(frame, frame_hw, 0)) != 0)
				{
					std::cout << "av_hwframe_transfer_data failed with " << ret << std::endl;
					return false;
				}
				out_frame = frame;
			}
		}

		*got_output = true;

		convert_to_bgr();

		current_frame++;
		// show_frame();
	}

	return true;
}
